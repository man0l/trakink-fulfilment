var user = { name: '', address: '', address2: ''};
 

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    switch(request.action)
    {
        case 'sendAddress':            
            
            // first clear the local storage            
            console.log("clear local storage");
            
            var phone        = localStorage['phone'];
            var walmartPhone = localStorage['walmart_phone'];
            var giftMsg      = localStorage['gift_msg'];

            localStorage.clear();

            localStorage['phone']           = phone;
            localStorage['walmart_phone']   = walmartPhone;
            localStorage['gift_msg']        = giftMsg;
            
            localStorage['buyer']           = request.buyer;
            localStorage['address']         = request.address;
            localStorage['address2']        = request.address2;
            localStorage['id']              = request.id;
            localStorage['eBayPrice']       = request.eBayPrice;
            localStorage['salesTax']        = request.salesTax;
            localStorage['minLostAmount']   = request.minLostAmount;
            localStorage['itemURL']         = request.itemURL;             
            
            localStorage['paypalFee']       = request.paypalFee;             
            localStorage['finalValueFee']   = request.finalValueFee;             
            
            console.log('buyer stored');
            
            sendResponse({}); 
        break;
        
        case 'getAddress':
            
             console.log('buyer get');             
             var show = calculateComplete(localStorage['price']);
             var response =  { 
                                buyer: localStorage['buyer'],  
                                address: localStorage['address'], 
                                address2: localStorage['address2'],
                                id:  localStorage['id'], 
                                phone: localStorage['phone'],
                                walmartPhone: localStorage['walmart_phone'],
                                complete: show,
                                itemURL: localStorage['itemURL']
                            };
             sendResponse(response);
             console.log(response);
        break;
        
        case 'savePrice':
            
            var show = calculateComplete(request.price);
            
            sendResponse({complete: show });
            chrome.tabs.query( { url: "http://auto.trak.ink/*" }, function(tabs) {
               console.log("sending msg to tab");
               chrome.tabs.sendMessage(tabs[0].id, { price: localStorage['price'], id:  localStorage['id'] }, function(response) {});
            });
        break;
        
        case 'saveOrderID':
            localStorage['orderID'] = request.orderID;
            sendResponse({});
            
            chrome.tabs.query( { url: "http://auto.trak.ink/*" }, function(tabs) {
               console.log("sending msg to tab");

               chrome.tabs.sendMessage(tabs[0].id, { price: localStorage['price'], id:  localStorage['id'], orderID: localStorage['orderID'] }, function(response) {
                    
               });
            });
         
            
        break;
        
        case 'getOrderDetails':
            sendResponse({ price: localStorage['price'], id:  localStorage['id'], orderID: localStorage['orderID'] });            
        break; 
        
        case 'getGiftMessage':
            sendResponse({ giftMsg: localStorage['gift_msg'] });            
            break;
        case "calcComplete":
            var show = calculateComplete(request.price);
            sendResponse({ complete: show });
            break;
    }
    
    return true;
});

function savePhone(phone) {
    localStorage['phone'] = phone;    
}

function savePriceAuto(priceAuto)
{
    localStorage['price_auto'] = priceAuto;
}
          

function saveGiftMsg(giftMsg)
{
    localStorage['gift_msg'] = giftMsg;
}

function saveWalmartPhone(phone)
{
    localStorage['walmart_phone'] = phone;
}

               
function calculateComplete(price) 
{
    localStorage['price'] = price;
    //var profit =  parseFloat(localStorage['eBayPrice']) - parseFloat((parseFloat(localStorage['eBayPrice']) * localStorage['eBayFee']) + parseFloat(localStorage['price']) + 0.3) + parseFloat(localStorage['salesTax']) - (parseFloat(localStorage['salesTax']) * localStorage['paypalFee']);
    var profit = parseFloat(localStorage['eBayPrice']) - parseFloat(price) - parseFloat(localStorage['finalValueFee']) - parseFloat(localStorage['paypalFee']) + parseFloat(localStorage['salesTax']);
    profit = profit.toFixed(2);

    var show = '';
    if(profit == 'NaN') {
            show = '';
    }
    else if(profit <= -(localStorage['minLostAmount'])) {
            show = 'No';	
    }
    else if(profit >= -(localStorage['minLostAmount'])) {
            show = 'Yes';	
    }
    
    return show;
}
 