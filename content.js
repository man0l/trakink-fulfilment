var user = { name: '', address: '', address2: ''};
 
 $(document).ready(function() {
     
       if(window.location.href.match(/auto/))
       {
           console.log('auto.trak.ink page');
           
            $(".supplier_link a").on('click', function() {
                 
                var buyerName     = $(this).parent().parent().find('.buyer_name').text().trim();
                var address       = $(this).parent().parent().find(".address1").text().trim();
                var address2      = $(this).parent().parent().find(".address2").text().trim();
                var id            = $(this).parent().attr('data-id');
                var minLostAmount = parseFloat($("#min_lost_opt").val());
                var eBayPrice     = parseFloat($("#ebay_price_"+id).val(),10);
                var salesTax      = parseFloat($("#sale_tax_"+id).val(),10);
                var itemURL       = $(this).attr("data-url");                
                var isTopRated    = $("#is_top_rated").val();
                var paypalFee     = parseFloat($("#paypal_fee_" + id).val());
                var finalValueFee = parseFloat($("#final_value_fee_" + id).val());
                var handlingTime  = parseInt($("#handling_time_" + id).val());
                
                finalValueFee = (isTopRated === "Yes" && handlingTime === 1) ? finalValueFee * 0.8 : finalValueFee;                
                sendAddress(buyerName, address, address2, id, eBayPrice, salesTax, minLostAmount, itemURL, paypalFee, finalValueFee);                  
                
                
            });
            
            
            chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {
                
                console.log("reciving msg to tab");
                
                calculateComplete(msg.price, msg.id);
                $("#supp_price_" + msg.id).val(msg.price);
 
                if (msg.orderID) {

                    if(!msg.priceAuto)
                    {
                        $("#supp_price_" + msg.id).val(msg.price);    
                    }                            
                    $("#supplier_order_id" + msg.id).val(msg.orderID);      


                  sendResponse({});
                }

            });

             
            
       } else if(window.location.href.match(/amazon/)) {
           
           
           $(document).on("keydown", function(e) {
               if ( e.altKey && ( String.fromCharCode(e.which).toLowerCase() === '1') ) {
                    console.log( "Filling address");
                    
					$("#addressChangeLinkId").trigger("click");
					
                    
                    chrome.runtime.sendMessage({action: 'getAddress'}, function(response) {
                      
                      
                 
                      $("#enterAddressFullName").val(response.buyer);
                      $("#enterAddressAddressLine1").val(response.address);
                     
                      var addressInfo = response.address2.split("\n");
                      $("#enterAddressCity").val(addressInfo[0].trim());
                      $("#enterAddressStateOrRegion").val(addressInfo[1].trim());
                      $("#enterAddressPostalCode").val(addressInfo[2].trim());
                      $("#enterAddressPhoneNumber").val(response.phone);
                      
                      $(".a-popover-footer .a-button-input").trigger('click');
                      
                    });
                } else if ( e.altKey && ( e.which === 192) ) {
					$(document).scrollTop($("div[aria-label='Payment method']").offset().top);
					
				}
           });
           
            $(document).on("keydown", function(e) {
                if ( e.altKey && ( String.fromCharCode(e.which).toLowerCase() === '3') ) {
                    console.log( "storing price");
                    var price = null;
                    
                    $('div#subtotals-marketplace-table table tbody tr td').each(function() {
                       if($(this).text().trim() == 'Total:')
                       {
                          price = $(this).parent().children().eq(1).text().trim().replace("$", "");
                          chrome.runtime.sendMessage({ action: 'savePrice', price: price}, function (response) {
                            // show the price in the 
                          });
                       }
                         
                    });
                    
                    // other layout
                   var price = document.querySelector("#subtotals-marketplace-table > table > tbody > tr:nth-child(7) > td.a-color-price.a-size-medium.a-text-right.a-align-bottom.aok-nowrap.grand-total-price.a-text-bold");
                   
                   
                   
                   if(price)
                   {
                       price = price.textContent.trim().replace("$", "");
                       chrome.runtime.sendMessage({ action: 'savePrice', price: price}, function (response) {} );
                   }
                    
                } 
                
            });
          
          
          $(document).on('keydown', function (e) {
              if ( e.altKey && ( String.fromCharCode(e.which).toLowerCase() === '2') ) {
                    console.log('filling gift message');
                    
                       chrome.runtime.sendMessage({ action: 'getGiftMessage' }, function (response) {
                           $("#message-area-0").val(response.giftMsg);
                           $("#giftForm span.primary-action-button input").trigger('click');
                       });
                }
          });
           
           
           if(window.location.href.match(/thankyou/))
           {
               
               var orderID = $('span[id^="order-number-"]').text().trim();                
               chrome.runtime.sendMessage({ action: 'saveOrderID', orderID: orderID }, function (response) {} );
               
               console.log(' Order ID Sent');

           }
           
       } else if(window.location.href.match(/walmart/)) {
           
           $(document).on("keydown", function(e) {
               if ( e.altKey && ( String.fromCharCode(e.which).toLowerCase() === '1') ) {
                     fillAddressWalmart();
                }
           });
            
            var interval = setInterval(function() {
                
                if(document.readyState === 'complete') {
                    clearInterval(interval);
                    
                    if(window.location.href.indexOf('checkout') >=  0 && window.location.href.indexOf('thankyou') < 0)
                    {
                        stickyFooter();
                        proceedCheckout();
                        
                    } else if(window.location.href.indexOf('thankyou') >= 0)
                    {
                        
                            //var orderID = $(".thankyou-main-heading").text().trim().split("#")[1];
                            
                            observeThankYou();
                            

                    }
                    
                }    
            }, 100);

           if(window.location.href.match(/veh=aff/))
           {
               // load the item URL
               setTimeout(loadItemURL, 1000);
           }
     
       }  else if(window.location.href.match(/homedepot/)) {
		   
		   $(document).on("keydown", function(e) {
               if ( e.altKey && ( String.fromCharCode(e.which).toLowerCase() === '1') ) {
                     fillAddressHomedepot();
                }
           });
	   } else if(window.location.href.match(/lowes/)) {
		   
		   $(document).on("keydown", function(e) {
               if ( e.altKey && ( String.fromCharCode(e.which).toLowerCase() === '1') ) {
                     fillLowes();
                }
           });
	   }
       
       
      
 });
 
 function loadItemURL() 
 {
     chrome.runtime.sendMessage({action: 'getAddress'}, function(response) {
        window.location.href = response.itemURL; 
     });
 }
 
 function getWalmartOrderID() 
 {
     
     if(window.location.href.indexOf('thankyou') >= 0)
     {    
         var match = $(".js-thankyou-main-heading").text().trim().match(new RegExp(/[0-9]+/));
         if(!match) {
             return "";
         }
         
         var orderID = match[0];	 
         return orderID = orderID.slice(0,orderID.indexOf(orderID.slice(-6))) + "-" + orderID.slice(-6);
	  
     }

    return "";
 }
 
 function observeThankYou()
 {
    var observer = new MutationObserver(function(mutations) {
     mutations.forEach(function(mutation) {
       if (!mutation.addedNodes) return

       for (var i = 0; i < mutation.addedNodes.length; i++) {
         // do things to your newly added nodes here
         var node = mutation.addedNodes[i];
         var loaded = false;
         
         if(!node.children)
             break;
         
            for( var j = 0; j < node.children.length; j++) 
            {  
               var node1 = node.children[j];
               if(node1.attributes.length > 0) {
                    
                    if(node1.getAttribute("class"))
                    {
                        loaded = node1.getAttribute("class").indexOf("thankyou-print-header") >= 0;                        
                        if(loaded)
                        {
                            stickyFooter(".thankyou-footer", true);
                            var orderID = getWalmartOrderID();
                            chrome.runtime.sendMessage({ action: 'saveOrderID', orderID: orderID }, function (response) {} );               
                            console.log(' Order ID Sent ' + orderID);                  
                            
                             
                            break;
                        }                    
                    }
               }               
               
            }
         
       }
     })
   });

   observer.observe(document.body, {
       childList: true
     , subtree: true
     , attributes: false
     , characterData: false
   });

   // stop watching using:
   //observer.disconnect()
    
 }
 
 function proceedCheckout() 
 {
     
    var checkoutShippingInterval = setInterval(function() {
        //var $divAddress = $("[data-automation-id='shipping-address'] div");
        var $divAddress = $("[data-automation-id='shipping-address'] div.CXO_module_header");
        if($divAddress.length)
        {
            console.log('---- container observe class name ----');            
            observeClassChanged('current', fillAddressWalmart, $divAddress); 
            clearInterval(checkoutShippingInterval);
        }
    }, 200);
    
    var checkoutPaymentInterval = setInterval(function() {
        var $div = $("[data-automation-id='payment'] div.CXO_module_header");
        if($div.length)
        {
            observeClassChanged('current', getTotalPrice, $div);
            clearInterval(checkoutPaymentInterval);
        }
    }, 200);

    switch(window.location.href.split("#/")[1])
    {
        case "payment":
            getTotalPrice();
            break;
        case "shipping-address":
            fillAddressWalmart();
            break;
        default:
            break;
    }
 }
 
 
 function observeClassChanged(myClass, myCallback, node)
 {
     
       
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                if (mutation.attributeName === "class") {
                    var attributeValue = $(mutation.target).prop(mutation.attributeName);
                    console.log("Class attribute changed to:", attributeValue);
                    if(attributeValue.indexOf(myClass) >= 0)
                    {
                         myCallback();
                    }
                }
            });
        });
        
        var myNode = (node.length > 0 ? node[0] : node);
        observer.observe(myNode,  {
          attributes: true
        });

 }
 
 
 function fillAddressWalmart()
 {
       console.log( "Filling address walmart");

       $("#addressChangeButtonId input").trigger('click');

       chrome.runtime.sendMessage({action: 'getAddress'}, function(response) {
           
           // check for existence
           var exists = false;
           $(".recipient-name").each(function() {
               if($(this).text().trim() == response.buyer.trim())
               {
                   exists = true;
               }
           });
           
           
           
           if(!exists)
           {
                      
                $("[data-tl-id='COAC2ShpAddrAddNewAddrLnk']").trigger('click');

                var buyerNames = response.buyer.split(new RegExp(/\s{1,3}/));
                $("[data-tl-id='COAC2ShpAddrFirstName']").focus().val(buyerNames[0]).trigger("change").blur();     
                buyerNames.splice(0,1);
                $("[data-tl-id='COAC2ShpAddrLastName']").focus().val(buyerNames.join(" ")).trigger("change").blur();
                if(response.walmartPhone)
                {
                    $("[data-tl-id='COAC2ShpAddrPhone']").focus().val(response.walmartPhone).trigger("change").blur();
                }   

                $("[data-tl-id='COAC2ShpAddrAddress1']").focus().val(response.address).trigger("change").blur();


                var addressInfo = response.address2.split("\n");
                var zipCode     = addressInfo[2].trim().split("-");
                $("[data-tl-id='COAC2ShpAddrCity']").focus().val(toTitleCase(addressInfo[0].trim())).trigger("change").blur();
                $("[data-tl-id='COAC2ShpAddrState']").focus().val(addressInfo[1].trim()).trigger("change").blur();            
                $("[data-tl-id='COAC2ShpAddrZip']").focus().val(zipCode[0]).trigger("change").blur();
                $("[data-tl-id='COAC2ShpAddrUseThisAddrBtn']").trigger('click');

           } else {
              $("[data-automation-id='address-form-on-cancel']").trigger('click');
           }


       });
  }   
  
function fillAddressHomedepot()
{
	console.log( "Filling address homedepot");
	chrome.runtime.sendMessage({action: 'getAddress'}, function(response) {
		
				var addressContainer = $("[name='addressFieldForm']");
                var buyerNames = response.buyer.split(new RegExp(/\s{1,3}/));
				
				addressContainer.find("input[name='firstName']").focus().val(buyerNames[0]).trigger("change").blur();     
				buyerNames.splice(0,1);
                addressContainer.find("input[name='lastName']").focus().val(buyerNames.join(" ")).trigger("change").blur();		 		

				addressContainer.find("input[id='shippingAddress']").focus().val(response.address).trigger("change").blur();                


                var addressInfo = response.address2.split("\n");
                var zipCode     = addressInfo[2].trim().split("-");
				
				addressContainer.find("input[name='zip']").focus().val(zipCode[0]).trigger("change").blur();
				
				/*
                $("[data-tl-id='COAC2ShpAddrCity']").focus().val(toTitleCase(addressInfo[0].trim())).trigger("change").blur();
                $("[data-tl-id='COAC2ShpAddrState']").focus().val(addressInfo[1].trim()).trigger("change").blur();            
                $("[data-tl-id='COAC2ShpAddrZip']").focus().val(zipCode[0]).trigger("change").blur();
                $("[data-tl-id='COAC2ShpAddrUseThisAddrBtn']").trigger('click');
				*/


       });
	
}

function fillLowes()
{
	console.log( "Filling address lowes");
	chrome.runtime.sendMessage({action: 'getAddress'}, function(response) {
		
				var addressContainer = $("#billing-address-edit");
                var buyerNames = response.buyer.split(new RegExp(/\s{1,3}/));
				
				addressContainer.find("input[id^=fname]").focus().val(buyerNames[0]).trigger("change").blur();     
				buyerNames.splice(0,1);
                addressContainer.find("input[id^=lname]").focus().val(buyerNames.join(" ")).trigger("change").blur();		 		

				addressContainer.find("input[id^='address-1']").focus().val(response.address).trigger("change").blur();                


                var addressInfo = response.address2.split("\n");
                var zipCode     = addressInfo[2].trim().split("-");
				
				addressContainer.find("input[id^='zip']").focus().val(zipCode[0]).trigger("change").blur();
				addressContainer.find("input[id^='city']").focus().val(addressInfo[0].trim()).trigger("change").blur();
				addressContainer.find("select[id^='state']").focus().val(addressInfo[1].trim()).trigger("change").blur();     
				
                /*$("[data-tl-id='COAC2ShpAddrCity']").focus().val(toTitleCase(addressInfo[0].trim())).trigger("change").blur();
                $("[data-tl-id='COAC2ShpAddrState']").focus().val(addressInfo[1].trim()).trigger("change").blur();            
                $("[data-tl-id='COAC2ShpAddrZip']").focus().val(zipCode[0]).trigger("change").blur();
                $("[data-tl-id='COAC2ShpAddrUseThisAddrBtn']").trigger('click');
				*/

				$(".saveProductAddress").trigger('click');
				

       });
	
}
  
  function toTitleCase(str)
  {
     return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  }


 function getTotalPrice()
 {
     
     $("#payment-option-radio-2").trigger("click");
     
     price = $(".price-display").text().trim().replace("$", "");
     chrome.runtime.sendMessage({ action: 'savePrice', price: price}, function (response) {
         console.log("get total price has been invoked" + response.complete);
         renderComplete(response, true, $("#trakink-footer"));
     });

 }
 
 function renderComplete(response, newContainer = false, appendToContainer = null) 
 {
     if(typeof(response.complete) !== "undefined" && response.complete == "")
     {
         return;
     }
     
    var completeColor = (response.complete == "Yes" ? "#76c143" : "#e92c42");

    if(!newContainer) {
        
        $(".complete-order").html('Complete: ');
        $("<span>").css({ color: completeColor, fontSize: "1.25rem" }).html(response.complete).appendTo(".complete-order");
        
    } else {
        
        var completeContainer = $(".complete-order");
        
        if(completeContainer.length) {
            completeContainer.html("Complete: ");
        } else {
            var completeContainer = $("<div>").addClass('complete-order').html("Complete: ");
        }
        
        if(appendToContainer.length)
        {
            $("<span>").css({ color: completeColor, fontSize: "1.25rem" }).html(response.complete).appendTo(completeContainer);
            appendToContainer.append(completeContainer);
        } 
    }
        
        

 }
 
 function stickyFooter(appendContainer = ".checkout-responsive-container", before = false) { 
     
    chrome.runtime.sendMessage({action: 'getAddress'}, function(response) {
        
        if(!response.buyer && !response.address && !response.address2)
        {
            return;
        }           
            
        var footer =  $("<div>");
        var complete  = response.complete ? "Complete:" + response.complete : "";
		
		if(getWalmartOrderID()) {
			var orderID   = "Order #" + getWalmartOrderID();
		}
        
        if(before)
        {
            footer.attr("id","trakink-footer").prependTo($(appendContainer).last());
        } else {
            footer.attr("id","trakink-footer").appendTo($(appendContainer).last());
        }
        
        $("<div>").addClass('address-sticky-footer').html(response.buyer + "<br>\n" + response.address + "<br>\n" + response.address2.split("<br>\n").join(" ")).appendTo(footer);
        //$("<div>").addClass('complete-order').html(complete).appendTo(footer);
        renderComplete(response, true, footer);
        $("<div>").addClass('order-id').html(orderID).appendTo(footer);         
        
    });
}

function sticky(footer) {
    //$(footer).sticky({ topSpacing: $(document).scrollTop()  });
}
 
 
 function sendAddress(buyerName, address, address2, id, eBayPrice, salesTax, minLostAmount, itemURL, paypalFee, finalValueFee) {
     
     chrome.runtime.sendMessage({ action: 'sendAddress', buyer: buyerName, address: address, address2: address2, id: id, eBayPrice: eBayPrice, salesTax: salesTax, minLostAmount: minLostAmount, itemURL: itemURL, paypalFee: paypalFee, finalValueFee: finalValueFee }, function(response) {
       
     });
 }
 
 
function calculateComplete(price, id) 
{
    
   chrome.runtime.sendMessage({ action: 'calcComplete', price: price }, function(response) {
        
        if($(".price-complete-container").length)
        {
            var completeContainer = $(".price-complete-container");
        } else {
            var completeContainer = $("<span>").addClass("price-complete-container");
        }
        
        response.complete == "Yes" ? completeContainer.css({ "color": "green" }) : completeContainer.css({ "color": "red" });
        completeContainer.html(response.complete);
        $("#supp_price_" + id).val(price);
        $("#supp_price_" + id).parent().append(completeContainer).css({ textAlign: "center", fontWeight: "bold"});

    });
} 


 
          
