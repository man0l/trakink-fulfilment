// Saves options to chrome.storage
function save_options() {
   
  var phone = document.getElementById('phone').value;
  var walmartPhone = document.getElementById('walmart_phone').value;

  var priceAuto = document.getElementById('price_auto').checked;
  var giftMsg = document.getElementById('gift_message').value;

  chrome.extension.getBackgroundPage().savePhone(phone);
  chrome.extension.getBackgroundPage().saveWalmartPhone(walmartPhone);
  chrome.extension.getBackgroundPage().savePriceAuto(priceAuto);
  chrome.extension.getBackgroundPage().saveGiftMsg(giftMsg);

  chrome.storage.sync.set({
    priceAuto: priceAuto,
    phone: phone,
    walmartPhone: walmartPhone,
    giftMsg: giftMsg
  }, function() {
      
    
    // Update status to let user know options were saved.
    var status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 750);
  });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
  
  chrome.storage.sync.get({
    priceAuto: true,
    phone: '',
    walmartPhone: '',
    giftMsg: ''
  }, function(items) {
     
    document.getElementById('phone').value = items.phone;
    document.getElementById('gift_message').value = items.giftMsg;
    document.getElementById('price_auto').checked = items.priceAuto;
    document.getElementById('walmart_phone').value = items.walmartPhone;

  });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',
    save_options);
