function saveOptions() {
   
  var orderID = document.getElementById('orderID').value;
  var price = document.getElementById('price').value;
 
  if(price && orderID)
  {
   chrome.runtime.sendMessage({ action: 'savePrice', price: price}, updateStatus );
   chrome.runtime.sendMessage({ action: 'saveOrderID', orderID: orderID }, updateStatus );
  } else {
      updateStatus(null, 'Please add both price and order #');
  }
    
}

function updateStatus(response, msg) {
    var status = document.getElementById('status');
    if(!msg) {
            status.textContent = 'The order details has been saved.';
            status.className = 'success'

    } else {
        status.textContent = msg;
        status.className = 'error';
    }
    
    setTimeout(function() {
      status.textContent = '';
    }, 2000);
}

 document.addEventListener('DOMContentLoaded', function() {
     
   document.getElementById('save').addEventListener('click', saveOptions);    
     
 });
